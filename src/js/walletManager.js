var express = require('express');
var mongodb = require('mongodb').MongoClient;



module.exports = {

    setWalletInfo: function (user, update, req, res) {
        var maximumLimitvalue = 0;
        //        var maximumLimit;
        //        var availableCredit;
        //        var cards = user.cards;
        //        for (var i = 0; i < cards.length; i++) {
        //            maximumLimitvalue += (cards[i].cardCreditLimit) * 1;
        //            availableLimitvalue += (cards[i].cardCreditLimit - cards[i].cardDebit) * 1;
        //
        //        }

        maximumLimitvalue = user.userMaximumLimit;
        var limit = user.userLimitSet;

        if (limit > maximumLimitvalue) {
            //            this.setLimit(req.user, maximumLimitvalue, update, req, res);
            user.userLimitSet = user.userMaximumLimit;
        }

        res.render('wallet', {
            maximumLimit: req.user.userMaximumLimit,
            availableCredit: user.userAvailableCredit,
            limitSet: req.user.userLimitSet
        });
    },


    setLimit: function (user, value, update, req, res, next) {

        if (value <= user.userMaximumLimit) {
            next(null, function () {
                user.userLimitSet = value * 1;
                update(req.user, req, res);
                res.redirect('/wallet');
            })
        } else if (value > user.userMaximumLimit) {
            user.userLimitSet = user.userMaximumLimit * 1;
            update(req.user, req, res);
            next(new Error('This value is out of your Cards Maximum Limit.'));
            //            res.redirect('/wallet/errorPage');
        }


    },

    makeAPurchase: function (user, value, req, res, next) {
        var creditAvailable = 0;
        var walletlimitAvailable = user.userLimitSet - (user.userMaximumLimit - user.userAvailableCredit);
        //        if (value > user.userLimitSet) {
        //            console.log('dentro valor>1');
        //            res.redirect('/wallet/errorPage');
        //            //            res.send('Purchase Value bigger than your Limit Set.');
        //        } else if (value > req.body.availableCredit) {
        //            console.log('dentro valor>2');
        //            res.redirect('/wallet/errorPage');
        //            //            res.send('Purchase Value bigger than your Available Limit.');
        //        } else {
        //            //        console.log(req.body.availableCredit);

        if (value > user.userLimitSet) {
            next(new Error('This value is out of your Wallet Limit Set.'));

        } else if (value > walletlimitAvailable) {
            next(new Error('This value is out of your Wallet Limit Available.'));

        } else {
            next(null, function () {
                var i = 0;
                while (value > 0) {
                    creditAvailable = user.cards[i].cardCreditLimit - user.cards[i].cardDebit;
                    if (value <= creditAvailable) {
                        user.cards[i].cardDebit += value * 1;
                        value = 0;
                    } else {
                        user.cards[i].cardDebit += creditAvailable * 1;
                        value -= creditAvailable * 1;
                    }
                    i += 1;
                }
                res.redirect('/wallet');
            });
        }
        //    }
    }
};
