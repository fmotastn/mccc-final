var express = require('express');
var signUpRouter = express.Router();

var router = function () {

    signUpRouter.route('/')
        .get(function (req, res) {
            res.render('signUp');
        });



    signUpRouter.route('/userSignedUp')
        .post(function (req, res) {
            console.log(req.body);
            res.render('index');
        });

    return signUpRouter;

};

module.exports = router();
