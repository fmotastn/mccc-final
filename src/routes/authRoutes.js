var express = require('express');
var authRouter = express.Router();
var mongodb = require('mongodb').MongoClient;
var passport = require('passport');

var router = function () {

    authRouter.route('/')
        .get(function (req, res) {
            var error = '';
            res.render('signUp', {
                error
            });
        });

    authRouter.route('/signUp')
        .post(function (req, res) {
            console.log(req.body);

            if (req.body.userPassword === req.body.userRepeatPassword) {
                var url =
                    'mongodb://localhost:27017/libraryApp';
                mongodb.connect(url, function (err, db) {
                    var collection = db.collection('users');
                    var user = {
                        userName: req.body.userName,
                        userPassword: req.body.userPassword,
                        userMaximumLimit: 0,
                        userLimitSet: 0,
                        cards: []
                    };
                    collection.insert(user, function (err, results) {
                        req.login(results.ops[0], function () {
                            res.redirect('/wallet');
                        });
                    });
                });
            } else {
                var error = ' - Passwords do not match, please try again';
                res.render('signUp', {
                    error
                });
            }
        });

    authRouter.route('/signIn')
        .post(passport.authenticate('local', {
                failureRedirect: '/'
            }),
            function (req, res) {
                res.redirect('/wallet');
            });

    authRouter.route('/signOut')
        .get(function (req, res) {
            req.logout();
            res.redirect('/');
        });

    authRouter.route('/profile')
        .get(function (req, res) {
            res.json(req.user);
        });

    return authRouter;
};

module.exports = router();
