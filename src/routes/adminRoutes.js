var express = require('express');
var adminRouter = express.Router();
var mongodb = require('mongodb').MongoClient;


var cards = [
    {
         cardName: 'Master',
        cardNumber: '1234123412341234',
        cardDueDate: '12/15',
        cardExpDate: '12/25',
        cardCVV: '734',
        cardLimit: '12000',
        read: false
        }
    ];

var router = function () {

    adminRouter.route('/addCards')
        .get(function (req, res) {
            var url = 'mongodb://localhost:27017/libraryApp';
            mongodb.connect(url, function (err, db) {
                var collection = db.collection('cards');
                collection.insertMany(cards, function (err, results) {
                    res.send(results);
                    db.close();
                });
            }); 
        });
    return adminRouter;
};


module.exports = router;
