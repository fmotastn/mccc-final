var express = require('express');
var walletRouter = express.Router();
var mongodb = require('mongodb').MongoClient;
var walletManager = require('D:/library/src/js/walletManager');
var maximumLimit = walletManager.maximumLimit;
var limit;

var cardType = function (cardNumber) {

    var result = 'Unknown';

    //first check for MasterCard
    if (/^5[1-5]/.test(cardNumber)) {
        result = 'Master Card';
    }

    //then check for Visa
    else if (/^4/.test(cardNumber)) {
        result = 'Visa';
    }

    //then check for AmEx
    else if (/^3[47]/.test(cardNumber)) {
        result = 'Amex';
    }

    return result;
};

var updateUser = function (user, req, res) {
    var availableLimitvalue = user.userAvailableCredit;
    var maximumLimit = user.userMaximumLimit;
    var limitSet = user.userLimitSet;
    var cards = user.cards;
    var url = 'mongodb://localhost:27017/libraryApp';
    mongodb.connect(url, function (err, db) {
        var collection = db.collection('users');
        collection.update({
                userName: user.userName
            }, {
                $set: {
                    userAvailableCredit: availableLimitvalue,
                    userMaximumLimit: maximumLimit,
                    userLimitSet: limitSet,
                    cards: cards
                }
            },

            function (err, results) {
                if (err) {
                    console.log(err);
                    res.redirect('/');
                } else {
                    var cards = user.cards;
                    db.close();

                }
            });
    });
};

var setCardPriority = function (user, req, res) {
    var date = new Date();
    var today = date.getDate();

    var cards = user.cards;
    for (card in cards) {
        if (parseInt(cards[card].cardDueDate) < today) {
            cards[card].cardDueDate = (parseInt(cards[card].cardDueDate)) + 30;
        };
    }
    cards.sort(function (cardA, cardB) {
        var result = Math.abs((cardB.cardDueDate - today)) - Math.abs((cardA.cardDueDate - today));
        if (result == 0)
            result = Math.abs((cardA.cardCreditLimit - today)) - Math.abs((cardB.cardCreditLimit - today));
        return result
    });
    for (card in cards) {
        if (parseInt(cards[card].cardDueDate) > 30) {
            cards[card].cardDueDate = (parseInt(cards[card].cardDueDate)) - 30;
        };
    }



};

var setMaximumAndAvailableLimit = function (user, req, res) {
    var maximumLimitvalue = 0;
    var availableLimitvalue = 0;
    var cards = user.cards;
    for (var i = 0; i < cards.length; i++) {
        maximumLimitvalue += (cards[i].cardCreditLimit) * 1;
        availableLimitvalue += (cards[i].cardCreditLimit - cards[i].cardDebit) * 1;
    }
    req.user.userAvailableCredit = availableLimitvalue;
    req.user.userMaximumLimit = maximumLimitvalue;
}


var router = function () {

    walletRouter.use(function (req, res, next) {
        if (!req.user) {
            res.redirect('/');
        } else {
            next();
        }
    });




    walletRouter.route('/')
        .get(function (req, res) {

            setMaximumAndAvailableLimit(req.user, req, res);
            walletManager.setWalletInfo(req.user, updateUser, req, res);
            updateUser(req.user, req, res);

        });

    walletRouter.route('/errorPage')
        .get(function (req, res) {
            var errormsg = 'Invalid value, please try again.';
            res.render('errorPage', {
                errormsg
            });
        });

    walletRouter.route('/setLimit')
        .post(function (req, res) {

            var value = 0;
            value = (req.body.setLimit);

            walletManager.setLimit(req.user, value, updateUser, req, res,
                function (err, result) {
                    // did an error occur?
                    if (err) {
                        res.render('errorPage', {
                            err
                        });
                    } else {
                        // no error occured, continue on
                        result();
                        res.redirect('/wallet');
                    }
                });
        });

    walletRouter.route('/makeAPurchase')
        .post(function (req, res) {

            setCardPriority(req.user);
            walletManager.makeAPurchase(req.user, req.body.purchase, req, res, function (err, result) {
                // did an error occur?
                if (err) {
                    res.render('errorPage', {
                        err
                    });
                } else {
                    // no error occured, continue on
                    result();
                }
            });


        });





    walletRouter.route('/cards')
        .get(function (req, res) {
            var cards = req.user.cards;
            res.render('cards', {
                cards
            });

        });

    walletRouter.route('/releaseCards')
        .get(function (req, res) {
            var cards = req.user.cards;
            res.render('releasecards', {
                cards
            });

        });



    walletRouter.route('/addCard')
        .get(function (req, res) {
            res.render('addCard');
        });


    walletRouter.route('/cardAdded')
        .post(function (req, res) {
            var card = req.body;
            card.cardType = cardType(card.cardNumber);
            card.cardDebit = 0;
            req.user.cards.push(card);
            updateUser(req.user, req, res);
            res.redirect('/wallet');

        });

    walletRouter.route('/removeCard')
        .post(function (req, res) {
            var radioId = req.body.radioId;
            req.user.cards.splice(radioId, 1);
            res.redirect('/wallet');
            updateUser(req.user, req, res);
        });

    walletRouter.route('/releaseCredit')
        .post(function (req, res) {
            var radioId = req.body.radioId;
            console.log(req.user.cards[radioId]);
            req.user.cards[radioId].cardDebit = 0;
            res.redirect('/wallet');
            updateUser(req.user, req, res);
        });


    return walletRouter;
};



module.exports = router();
