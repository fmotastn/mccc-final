var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    mongodb = require('mongodb').MongoClient,
    authRouter = require('D:/library/src/routes/authRoutes');

module.exports = function () {
    passport.use(new LocalStrategy({
            usernameField: 'userName',
            passwordField: 'userPassword'
        },
        function (username, password, done) { //check it is an appropriate sign in
            var url =
                'mongodb://localhost:27017/libraryApp';
            mongodb.connect(url, function (err, db) {
                var collection = db.collection('users');
                collection.findOne({
                        userName: username
                    },
                    function (err, results) {
                        if (results === null) {
                            return done(Error('Invalid login, please try again.'), null);
                        }
                        if (results.userPassword === password) {
                            var user = results; //it return the user if it exists in the db
                            done(null, user);
                        } else {
                            done(null, false);
                        }
                    }
                );
            });
        }));
};
