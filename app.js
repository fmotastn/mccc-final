var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var passport = require('passport');
var session = require('express-session');

var app = express();

var port = process.env.PORT || 5000;


var adminRouter = require('./src/routes/adminRoutes');
var authRouter = require('./src/routes/authRoutes');
var walletManager = require('./src/js/walletManager');

//var signUpRouter = require('./src/routes/signUpRoutes');
var walletRouter = require('./src/routes/walletRoutes');
//var mainjs = require('./public/js/main');

app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(session({
    secret: 'library'
}));
require('./src/config/passport')(app);

app.set('views', './src/views');

app.set('view engine', '.ejs');

//app.use('/Books', bookRouter);
app.use('/Admin', adminRouter);
app.use('/Auth', authRouter);

//app.use('/signUp', signUpRouter);
app.use('/wallet', walletRouter);
//app.use('/wallet', mainjs);

app.get('/', function (req, res) {
    res.render('index');
});

app.listen(port, function (err) {
    console.log('running server on port ' + port);
});



var MongoClient = require('mongodb').MongoClient,
    assert = require('assert');

// Connection URL
var url = 'mongodb://localhost:27017/myproject';
// Use connect method to connect to the Server
MongoClient.connect(url, function (err, db) {
    assert.equal(null, err);
    console.log('Connected correctly to server');

    db.close();
});
